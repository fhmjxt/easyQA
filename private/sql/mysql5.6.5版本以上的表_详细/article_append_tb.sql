CREATE TABLE `article_append_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `article_id` INT UNSIGNED NOT NULL COMMENT '文章id',
  `append_content` TEXT NOT NULL COMMENT '追加内容',
  `append_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '追加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `articleId` (`article_id`),
  INDEX `appendTime_index` (`append_time`)
) ENGINE=MYISAM COMMENT='文章追加表' CHARSET=utf8 COLLATE=utf8_general_ci;